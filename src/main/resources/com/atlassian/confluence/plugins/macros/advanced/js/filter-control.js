AJS.toInit(function ($) {

    $("select.content-filter").change(function() {
        $(".filter-control .waiting-image").removeClass("hidden");
        var $select = $(this);
        $.get(AJS.params.changesUrl, { "contentType" : $(this).val() }, function (data) {
            var $data = $(data);
            var $form = $select.parent();
            $form.parent().siblings(".results-container").children("ul").html($data);
            $(".waiting-image", $form).addClass("hidden");
            $(".more-link", $data).click(AJS.moreLinkClickHandler);
        });
    });

});
