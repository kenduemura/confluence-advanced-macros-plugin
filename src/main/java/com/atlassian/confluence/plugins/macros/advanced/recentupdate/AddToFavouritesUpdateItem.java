package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.i18n.I18NBean;

public class AddToFavouritesUpdateItem extends AbstractUpdateItem
{
    private final ContentEntityObject content;
    private final String username;

    public AddToFavouritesUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, ContentEntityObject content, String username, I18NBean i18n)
    {
        super(searchResult, dateFormatter, i18n, "content-type-favourite");
        this.content = content;
        this.username = username;
    }

    ///CLOVER:OFF
    @Override
    public Updater getUpdater()
    {
        return new DefaultUpdater(username, i18n);
    }

    @Override
    protected String getUpdateTargetUrl()
    {
        return content.getUrlPath();
    }

    @Override
    public String getUpdateTargetTitle()
    {
        return content.getDisplayTitle();
    }

    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.add.to.favourites";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.add.to.favourites";
    }
    ///CLOVER:ON

}
