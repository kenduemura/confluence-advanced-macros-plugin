package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import java.util.LinkedList;
import java.util.List;

/**
 * Performs grouping on the updater. Makes an exception for user status update items which are grouped separately on their own.
 */
public class DefaultGrouper implements Grouper
{
    private final LinkedList<Grouping> groupings;

    public DefaultGrouper()
    {
        this.groupings = new LinkedList<Grouping>();
    }

    public void addUpdateItem(UpdateItem updateItem)
    {
        if (updateItem == null)
            return;

        if (groupings.isEmpty() || !groupings.getLast().canAdd(updateItem))
        {
            if (updateItem instanceof UserStatusUpdateItem)
                groupings.add(new UserStatusGrouping(updateItem.getUpdater()));
            else
                groupings.add(new GenericGrouping(updateItem.getUpdater()));
        }

        groupings.getLast().addUpdateItem(updateItem);
    }

    public List<Grouping> getUpdateItemGroupings()
    {
        return groupings;
    }
}
