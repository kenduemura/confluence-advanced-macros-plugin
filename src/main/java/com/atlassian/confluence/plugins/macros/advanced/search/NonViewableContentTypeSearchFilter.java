package com.atlassian.confluence.plugins.macros.advanced.search;

import com.atlassian.confluence.search.v2.AbstractChainableSearchFilter;
import java.lang.Override;
import java.lang.String;


public class NonViewableContentTypeSearchFilter extends AbstractChainableSearchFilter
{
    private static final String KEY = "nonViewableContentTypeSearchFilter";

    private static NonViewableContentTypeSearchFilter instance = new NonViewableContentTypeSearchFilter();

    @Override
    public String getKey()
    {
        return KEY;
    }

    public static NonViewableContentTypeSearchFilter getInstance()
    {
        return instance;
    }
}
