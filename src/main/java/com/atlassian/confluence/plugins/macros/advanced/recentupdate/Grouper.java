package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import java.util.List;

/**
 * Performs grouping of {@link UpdateItem}s.
 * <p>
 * Typical usage would involve adding the required update items via {@link #addUpdateItem(UpdateItem)}, and retrieve
 * the groupings of them via {@link #getUpdateItemGroupings()}.
 */
public interface Grouper
{
    void addUpdateItem(UpdateItem updateItem);

    List<Grouping> getUpdateItemGroupings();
}
