package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.migration.LinkResolver;
import com.atlassian.confluence.content.render.xhtml.model.links.DefaultLink;
import com.atlassian.confluence.content.render.xhtml.model.links.NotPermittedLink;
import com.atlassian.confluence.content.render.xhtml.model.links.UnresolvedLink;
import com.atlassian.confluence.content.render.xhtml.model.resource.identifiers.*;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.Link;
import org.apache.commons.lang.StringUtils;

public class DefaultPageProvider implements PageProvider
{
    private LinkResolver linkResolver;
    private PermissionManager permissionManager;
    private I18NBeanFactory i18NBeanFactory;
    private BlogPostResourceIdentifierResolver blogPostResourceIdentifierResolver;
    private PageResourceIdentifierResolver pageResourceIdentifierResolver;

    public ContentEntityObject resolve(String location, ConversionContext context) throws NotAuthorizedException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        if (StringUtils.isEmpty(location))
        {
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.no.location"));
        }

        Link link = linkResolver.resolve(location, context.getPageContext());
        if (link == null)
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.no.link", new String[]{location}));
        return resolve(link, context);
    }

    @Override
    public ContentEntityObject resolve(Link link, ConversionContext context)
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();
        String location = getLocation(link.getDestinationResourceIdentifier());

        if (link instanceof NotPermittedLink)
            throw new NotAuthorizedException(i18NBean.getText("confluence.macros.advanced.include.error.user.not.authorized",
                    new String[]{AuthenticatedUserThreadLocal.getUsername(), location}));

        if (link instanceof UnresolvedLink || !(link instanceof DefaultLink))
            return null;

        AbstractPage page = resolveResourceIdentifier(link.getDestinationResourceIdentifier(), i18NBean, context);
        if (page == null)
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.no.link", new String[]{location}));


        // NOTE: UnpermittedLink will NOT check the user's permissions to view pages.
        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, page))
        {
            throw new NotAuthorizedException(i18NBean.getText(
                    "confluence.macros.advanced.include.error.user.not.authorized",
                    new String[]{AuthenticatedUserThreadLocal.getUsername(), location}
            ));
        }

        return page;
    }

    private String getLocation(ResourceIdentifier resourceIdentifier)
    {
        if (resourceIdentifier instanceof BlogPostResourceIdentifier)
        {
            BlogPostResourceIdentifier blog = (BlogPostResourceIdentifier) resourceIdentifier;
            return (blog.getSpaceKey() != null ? blog.getSpaceKey() + ":" : "") + "/" +
                BlogPost.toDatePath(blog.getPostingDay().getTime()) + "/" + blog.getTitle();
        }
        else if (resourceIdentifier instanceof PageResourceIdentifier)
        {
            PageResourceIdentifier page = (PageResourceIdentifier) resourceIdentifier;
            return (page.getSpaceKey() != null ? page.getSpaceKey() + ":" : "") + page.getTitle();
        }
        return resourceIdentifier.toString();
    }

    private AbstractPage resolveResourceIdentifier(ResourceIdentifier destination, I18NBean i18NBean, ConversionContext context)
    {
        if (!(destination instanceof PageResourceIdentifier || destination instanceof BlogPostResourceIdentifier))
            throw new IllegalArgumentException(i18NBean.getText("confluence.macros.advanced.include.error.invalid.content-entity"));

        try
        {
            if (destination instanceof BlogPostResourceIdentifier)
            {
                return blogPostResourceIdentifierResolver.resolve((BlogPostResourceIdentifier) destination, context);
            }

            return pageResourceIdentifierResolver.resolve((PageResourceIdentifier) destination, context);
        }
        catch (CannotResolveResourceIdentifierException e)
        {
            return null;
        }
    }

    public void setXhtmlMigrationLinkResolver(LinkResolver linkResolver)
    {
        this.linkResolver = linkResolver;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setBlogPostResourceIdentifierResolver(BlogPostResourceIdentifierResolver blogPostResourceIdentifierResolver)
    {
        this.blogPostResourceIdentifierResolver = blogPostResourceIdentifierResolver;
    }

    public void setPageResourceIdentifierResolver(PageResourceIdentifierResolver pageResourceIdentifierResolver)
    {
        this.pageResourceIdentifierResolver = pageResourceIdentifierResolver;
    }
}
