package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.spring.container.ContainerManager;

/**
 * {@link com.atlassian.confluence.plugins.macros.advanced.recentupdate.UserLink} that has the profile image of the user as the link body
 * instead of the full name.
 */
public class UserProfileLink extends UserLink
{
    final String staticResourceUrlPrefix;
    final ProfilePictureInfo profilePicture;

    public UserProfileLink(String username, I18NBean i18n)
    {
        super(username, i18n);
        this.profilePicture = userAccessor.getUserProfilePicture(user);
        this.staticResourceUrlPrefix = ((WebResourceUrlProvider) ContainerManager.getComponent("webResourceUrlProvider")).getStaticResourcePrefix(UrlMode.AUTO);
    }

    protected String getLinkBody()
    {
        if (user == null)
        {
            return String.format("<img class=\"userLogo logo anonymous\" src=\"%s/images/icons/profilepics/anonymous.png\" alt=\"\" title=\"%s\">",
                    staticResourceUrlPrefix, i18n.getText("user.icon.anonymous.title"));
        }
        else
        {
            return String.format("<img class=\"userLogo logo\" src=\"%s\" alt=\"\" title=\"%s\">", getImageSrc(),
                GeneralUtil.htmlEncode(username));
        }
    }

    @Override
    protected String getHref()
    {
        if (profilePicture.isDefault())
        {
            if (user == AuthenticatedUserThreadLocal.getUser())
                return String.format("%s/users/editmyprofilepicture.action", RequestCacheThreadLocal.getContextPath());
            else
                return "";
        }
        else
            return super.getHref();
    }

    private String getImageSrc()
    {
        if (profilePicture.isDefault())
        {
            if (user == AuthenticatedUserThreadLocal.getUser())
                return String.format("%s/images/icons/profilepics/add_profile_pic.png", staticResourceUrlPrefix);
            else
                return staticResourceUrlPrefix + profilePicture.getDownloadPath();
        }
        else
            return RequestCacheThreadLocal.getContextPath() + profilePicture.getDownloadPath();
    }
}
