package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.search.v2.ResultFilter;

public interface ResultFilterFactory<T extends ResultFilter>
{
    T getResultFilter();
}
