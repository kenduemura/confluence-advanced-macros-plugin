package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;

/**
 * This exists to serve AJAX requests for recent updates. Updates are served as separated results with no grouping.
 * At the moment, these are used to serve requests from the concise theme.
 */
public class ConciseThemeChangesAction extends AbstractChangesAction
{
    protected Theme getTheme()
    {
        return Theme.concise;
    }
}
