package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultGrouper;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Grouping;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.UpdateItem;

import java.util.List;

/**
 * This exists to serve AJAX requests for recent updates. Updates are grouped before they are returned. 
 * At the moment, these are used to serve requests from the social theme.
 */
public class SocialThemeChangesAction extends AbstractChangesAction
{
    private List<? extends Grouping> groupings;

    @Override
    public String execute() throws Exception
    {
        String superResult = super.execute();

        if (INPUT.equals(superResult))
            return superResult;

        DefaultGrouper grouper = new DefaultGrouper();
        for (UpdateItem updateItem : updateItems)
            grouper.addUpdateItem(updateItem);

        groupings = grouper.getUpdateItemGroupings();

        return SUCCESS;
    }

    protected Theme getTheme()
    {
        return Theme.social;
    }

    public List<? extends Grouping> getGroupings()
    {
        return groupings;
    }
}
