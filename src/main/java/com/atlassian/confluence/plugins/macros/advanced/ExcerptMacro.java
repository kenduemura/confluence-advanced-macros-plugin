package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * <p>Designate a certain part of a page to be the "excerpt". The body contents of the macro will now be
 * rendered to ensure consistency between page that has an excerpt and also other macros that include the
 * excerpt content. (CONF-6342)
 *
 * <p>Note that the actual content property stored with the page will be wiki markup. Other macros that include
 * the excerpt (such as the excerpt-include) will need to be render it with the correct context so that things
 * like links, images etc.. are rendered and displayed correctly.
 */
public class ExcerptMacro extends BaseMacro
{
    private ContentPropertyManager contentPropertyManager;

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.INLINE;
    }

    public boolean isInline()
    {
        return true;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return null;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        return shouldHideExcerpt(parameters) ? "" : body;
    }

    private boolean shouldHideExcerpt(Map parameters)
    {
        return "true".equalsIgnoreCase((String)parameters.get("hidden"));
    }
}
