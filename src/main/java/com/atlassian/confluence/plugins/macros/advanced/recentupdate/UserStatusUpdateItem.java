package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.userstatus.InlineWikiStyleRenderer;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

public class UserStatusUpdateItem extends AbstractUpdateItem
{
    private InlineWikiStyleRenderer inlineWikiStyleRenderer;

    public UserStatusUpdateItem(SearchResult searchResult, DateFormatter dateFormatter, I18NBean i18n, InlineWikiStyleRenderer inlineWikiStyleRenderer, String iconClass)
    {
        super(searchResult, dateFormatter, i18n, iconClass);
        this.inlineWikiStyleRenderer = inlineWikiStyleRenderer;
    }

    /**
     * Don't link statuses
     */
    @Override
    @HtmlSafe
    public String getLinkedUpdateTarget()
    {
        return inlineWikiStyleRenderer.render(searchResult.getDisplayTitle());
    }

    ///CLOVER:OFF
    public String getDescriptionAndDateKey()
    {
        return "update.item.desc.status";
    }

    public String getDescriptionAndAuthorKey()
    {
        return "update.item.desc.author.status";
    }
    ///CLOVER:ON

    public String getPermLink()
    {
        return RequestCacheThreadLocal.getContextPath()+searchResult.getUrlPath();
    }
}
