package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

public class UserStatusGrouping extends AbstractGrouping
{
    public UserStatusGrouping(Updater updater)
    {
        super(updater);
    }

    public boolean canAdd(UpdateItem updateItem)
    {
        if (!(updateItem instanceof UserStatusUpdateItem))
            return false;

        return updateItems.isEmpty(); // statuses can only be aded to a grouping on their own
    }
}
