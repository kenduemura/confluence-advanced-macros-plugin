package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.Link;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

/**
 * Utilities for PageIncludeMacro and ExcerptIncludeMacro only.
 *
 *
 */
public class IncludeMacroUtils
{
    public static Link getLink(ConversionContext conversionContext)
    {
        Link link = null;
        boolean oldFormatData = !conversionContext.hasProperty("macroDefinition");
        if (!oldFormatData)
        {
            MacroDefinition macroDefinition = (MacroDefinition) conversionContext.getProperty("macroDefinition");
            link = macroDefinition.getTypedParameter("", com.atlassian.confluence.xhtml.api.Link.class);
        }
        return link;
    }
}
