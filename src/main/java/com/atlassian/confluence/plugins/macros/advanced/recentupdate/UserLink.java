package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

/**
 * Represents a user link, encapsulating the handling of anonymous users and html link construction.
 */
public class UserLink
{
    final String username;
    final User user;
    final UserAccessor userAccessor;
    final I18NBean i18n;

    public UserLink(String username, I18NBean i18n)
    {
        this.username = username;
        this.userAccessor = ((UserAccessor) ContainerManager.getComponent("userAccessor"));
        this.user = userAccessor.getUser(username);
        this.i18n = i18n;
    }

    @Override
    public String toString()
    {
        return user != null ? String.format("<a class=\"%s\" data-username=\"%s\" href=\"%s\">%s</a>",
            getCssClass(), GeneralUtil.urlEncode(username), getHref(), getLinkBody()) : getLinkBody();
    }

    protected String getLinkBody()
    {
        if (user == null)
            return i18n.getText("anonymous.name");
        else
            return GeneralUtil.htmlEncode(user.getFullName());
    }

    protected String getHref()
    {
        return RequestCacheThreadLocal.getContextPath() + com.atlassian.confluence.links.linktypes.UserProfileLink.getLinkPath(username);
    }

    private String getCssClass()
    {
        PermissionManager permissionManager = (PermissionManager) ContainerManager.getComponent("permissionManager");

        if (permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, user))
            return "confluence-userlink url fn";
        else
            return "url fn";
    }
}
