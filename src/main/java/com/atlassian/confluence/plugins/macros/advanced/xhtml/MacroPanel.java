// NOTE - most of the code in this method is originally from AbstractPanelMacro. Until that class is removed, any 
// bugfixes in this class should be duplicated there.
package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.basic.CssSizeValue;
import com.atlassian.renderer.v2.macro.basic.validator.BorderStyleValidator;
import com.atlassian.renderer.v2.macro.basic.validator.ColorStyleValidator;
import com.atlassian.renderer.v2.macro.basic.validator.CssSizeValidator;
import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;
import com.atlassian.renderer.v2.macro.basic.validator.ValidatedMacroParameters;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * A panel with a title that wraps Macro content.
 *
 * @since 2.1.2 Recommended instead of the inheritance-based {@link PanelMacro}.
 */
public class MacroPanel
{
    public static String wrap(String title, String body, Map<String, String> parameters, RenderContext pageContext)
        throws MacroExecutionException
    {
        return wrap(title, body, parameters, pageContext, "panel", "panelContent", "panelHeader");
    }

    public static String wrap(String title, String body, Map<String, String> parameters,
        RenderContext pageContext, String panelCSSClass, String panelContentCSSClass, String panelHeaderCSSClass)
        throws MacroExecutionException
    {
        StringBuilder buffer = new StringBuilder(body.length() + 100);

        ValidatedMacroParameters validatedParameters = new ValidatedMacroParameters(parameters);
        validatedParameters.setValidator("borderStyle", BorderStyleValidator.getInstance());
        validatedParameters.setValidator("borderColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("bgColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("titleBGColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("borderWidth", CssSizeValidator.getInstance());

        String borderStyle;
        String borderColor;
        String backgroundColor;
        String titleBackgroundColor;
        String borderWidthString;
        try
        {
            borderStyle = validatedParameters.getValue("borderStyle");
            borderColor = validatedParameters.getValue("borderColor");
            backgroundColor = validatedParameters.getValue("bgColor");
            titleBackgroundColor = validatedParameters.getValue("titleBGColor");
            borderWidthString = validatedParameters.getValue("borderWidth");
        } catch (MacroParameterValidationException e)
        {
            throw new MacroExecutionException(e);
        }

        int borderWidth = 1;
        if (borderWidthString != null)
        {
            CssSizeValue cssBorderWidth = new CssSizeValue(borderWidthString);
            borderWidth = cssBorderWidth.value();
        }

        Map<String, String> explicitStyles = prepareExplicitStyles(borderWidth, borderStyle, borderColor, backgroundColor);

        if (StringUtils.isBlank(titleBackgroundColor) && StringUtils.isNotBlank(backgroundColor))
            titleBackgroundColor = backgroundColor;

        buffer.append("<div class=\"").append(panelCSSClass).append("\"");

        if (explicitStyles.size() > 0)
            handleExplicitStyles(buffer, explicitStyles);

        buffer.append(">");

        if (StringUtils.isNotBlank((title)))
            writeHeader(pageContext, buffer, title, borderStyle, borderColor, borderWidth, titleBackgroundColor, panelHeaderCSSClass);
        if (StringUtils.isNotBlank(body))
            writeContent(buffer, body, backgroundColor, panelContentCSSClass);

        buffer.append("</div>");

        return buffer.toString();
    }

    private static void handleExplicitStyles(StringBuilder buffer, Map<String, String> explicitStyles)
    {
        buffer.append(" style=\"");

        for (Iterator iterator = explicitStyles.keySet().iterator(); iterator.hasNext();)
        {
            String styleAttribute = (String) iterator.next();
            String styleValue = explicitStyles.get(styleAttribute);
            buffer.append(styleAttribute).append(": ").append(styleValue).append(";");
        }

        buffer.append("\"");
    }

    private static Map<String, String> prepareExplicitStyles(int borderWidth, String borderStyle, String borderColor,
        String backgroundColor)
    {
        Map<String, String> explicitStyles = new TreeMap<String, String>();

        explicitStyles.put("border-width", borderWidth + "px");
        if (borderWidth > 0)
        {
            if (StringUtils.isNotBlank(borderStyle))
                explicitStyles.put("border-style", borderStyle);
            if (StringUtils.isNotBlank(borderColor))
                explicitStyles.put("border-color", borderColor);
        }
        else
        {
            // hack to make borderless panels look right (the bottom margins of block level elements like <p> and <ul>
            // do NOT pad out the bottom of the panel body unless there is a border
            if (borderWidth == 0)
                explicitStyles.put("border-bottom", "1px solid white");
        }

        if (StringUtils.isNotBlank(backgroundColor))
            explicitStyles.put("background-color", backgroundColor);
        return explicitStyles;
    }

    private static void writeHeader(RenderContext renderContext, StringBuilder buffer, String title,
        String borderStyle, String borderColor, int borderWidth, String titleBackgroundColor,
        String panelHeaderCSSClass)
    {
        buffer.append("<div class=\"").append(panelHeaderCSSClass).append("\"").append(
            renderContext.isRenderingForWysiwyg() ? " wysiwyg=\"ignore\" " : "");

        buffer.append(" style=\"");

        buffer.append("border-bottom-width: ").append(borderWidth).append("px;");
        if (borderWidth > 0)
        {
            if (StringUtils.isNotBlank(borderStyle))
                buffer.append("border-bottom-style: ").append(borderStyle).append(";");
            if (StringUtils.isNotBlank(borderColor))
                buffer.append("border-bottom-color: ").append(borderColor).append(";");
        }
        if (StringUtils.isNotBlank(titleBackgroundColor))
            buffer.append("background-color: ").append(titleBackgroundColor).append(";");

        buffer.append("\"");
        buffer.append("><b>");
        buffer.append(StringEscapeUtils.escapeHtml(title));
        buffer.append("</b></div>");
    }

    private static void writeContent(StringBuilder buffer, String content, String backgroundColor,
        String panelContentCSSClass)
    {
        buffer.append("<div class=\"").append(panelContentCSSClass).append("\"");
        if (StringUtils.isNotBlank(backgroundColor))
            buffer.append(" style=\"background-color: ").append(backgroundColor).append(";\"");
        buffer.append(">\n");
        buffer.append(content.trim());
        buffer.append("\n</div>");
    }
}
