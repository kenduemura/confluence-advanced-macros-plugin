package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * A macro to display a page's change history.
 */
public class ChangeHistoryMacro extends BaseMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/changehistory.vm";
    private PageManager pageManager;

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        Map<String, Object> contextMap = getDefaultVelocityContext();

        PageContext context = (PageContext) renderContext;
        ContentEntityObject ceo = context.getEntity();

        if (ceo instanceof AbstractPage)
        {
            AbstractPage page = (AbstractPage) ceo;
            List previousVersions = pageManager.getVersionHistorySummaries(page);

            // Remove the current version, that's rendered separately
            previousVersions.remove(0);

            contextMap.put("page", page);
            contextMap.put("previousVersions", previousVersions);
            contextMap.put("space", page.getSpace());

            return getRenderedTemplate(contextMap);
        }
        else
        {
        	return RenderUtils.blockError(
    		        getConfluenceActionSupportTextStatic(),
    		        StringUtils.EMPTY);
        }
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }
    
	protected String getConfluenceActionSupportTextStatic() {
		return ConfluenceActionSupport.getTextStatic("changehistory.error.can-only-be-used-in-pages-or-blogposts");
	}

    ///CLOVER:OFF
	protected String getRenderedTemplate(Map<String, Object> contextMap) {
		return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);
	}

	protected Map<String, Object> getDefaultVelocityContext() {
		return MacroUtils.defaultVelocityContext();
	}
    ///CLOVER:OFF
}
