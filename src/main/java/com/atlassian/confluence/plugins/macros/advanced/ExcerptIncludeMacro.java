package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

public class ExcerptIncludeMacro extends BaseMacro
{
    // New macro to which this class delegates.
    private com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptIncludeMacro xhtmlMacro = new com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptIncludeMacro();

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.INLINE_BLOCK;
    }

    @Override
    public boolean isInline()
    {
        return true;
    }

    @Override
    public boolean hasBody()
    {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public void setLinkResolver(LinkResolver linkResolver)
    {
        xhtmlMacro.setLinkResolver(linkResolver);
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        xhtmlMacro.setPermissionManager(permissionManager);
    }

    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return xhtmlMacro.execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }

    @Override
    public boolean suppressMacroRenderingDuringWysiwyg()
    {
        return true;
    }

    public void setExcerptHelper(ExcerptHelper excerptHelper)
    {
        xhtmlMacro.setExcerptHelper(excerptHelper);
    }

    public void setViewRenderer(Renderer viewRenderer)
    {
        xhtmlMacro.setViewRenderer(viewRenderer);
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        xhtmlMacro.setI18NBeanFactory(i18NBeanFactory);
    }

    public void setLocaleManager(LocaleManager localeManager)
    {
        xhtmlMacro.setLocaleManager(localeManager);
    }

    public void setPageProvider(PageProvider pageProvider)
    {
        xhtmlMacro.setPageProvider(pageProvider);
    }
}
