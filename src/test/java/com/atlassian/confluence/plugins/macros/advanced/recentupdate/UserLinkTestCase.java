package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

public class UserLinkTestCase extends AbstractTestCase
{
    private String username = "Admin";
    private String contextPath = "/confluence";

    @Mock private UserAccessor userAccessor;
    @Mock private ConfluenceUser user;
    @Mock private PermissionManager permissionManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        when(userAccessor.getUser(username)).thenReturn(user);
    }

    @Override
    protected Map<String, ?> getAdditionalSingletons()
    {
        Map<String, Object> singletones = new HashMap<String, Object>();
        singletones.put("userAccessor", userAccessor);
        singletones.put("permissionManager", permissionManager);

        return singletones;
    }

    public void testConvertUserLinkToString()
    {
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, contextPath));

        UserLink userLink = new UserLink(username, i18n);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, user)).thenReturn(true);

        assertEquals("<a class=\"confluence-userlink url fn\" data-username=\"" + username + "\" " +
            "href=\"" + contextPath + "/display/~" + username + "\"></a>"
            , userLink.toString());
    }
}
