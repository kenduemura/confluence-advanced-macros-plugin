package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import static org.mockito.Mockito.mock;

public class UserStatusGroupingTestCase extends AbstractTestCase
{
    private UserStatusGrouping userStatusGrouping;

    public void testCanAddingNonStatusItemProhibited()
    {
        assertFalse(userStatusGrouping.canAdd(mock(ContentUpdateItem.class))); 
    }

    public void testCanOnlyAddOneStatusPerUserStatusGrouping() // this will allow statuses to appear on their own (as opposed to being grouped with other updates)
    {
        userStatusGrouping.addUpdateItem(mock(UserStatusUpdateItem.class));

        assertFalse(userStatusGrouping.canAdd(mock(UserStatusUpdateItem.class)));
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        userStatusGrouping = new UserStatusGrouping(new DefaultUpdater("fred", i18n));
    }
}
