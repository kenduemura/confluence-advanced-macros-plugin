package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.mockito.Mock;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RecentlyUsedLabelsMacroTestCase extends TestCase
{
    @Mock private LabelManager labelManager;
    @Mock private UserAccessor userAccessor;
    @Mock private FormatSettingsManager formatSettingsManager;
    @Mock private LocaleManager localeManager;
    @Mock private ConfluenceUserPreferences confluenceUserPreferences;

    private RecentlyUsedLabelsMacro recentlyUsedLabelsMacro;

    private Map<String, String> macroParams;

    private Space space;

    private Page pageToRenderOn;

    private Map<String, Object> macroVelocityContext;

    private ConfluenceUser user;

    private Label labelOne;

    private Label personalLabelOne;

    private ContentEntityObject labelledContent;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        when(userAccessor.getConfluenceUserPreferences((User) anyObject())).thenReturn(confluenceUserPreferences);
        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());
        when(formatSettingsManager.getDateFormat()).thenReturn("dd/MM/yyyy");
        when(formatSettingsManager.getDateTimeFormat()).thenReturn("dd/MM/yyyy HH:mm");
        when(formatSettingsManager.getTimeFormat()).thenReturn("HH:mm:ss SSS");
        when(localeManager.getLocale((User) anyObject())).thenReturn(Locale.UK);

        recentlyUsedLabelsMacro = new TestRecentlyUsedLabelsMacro();

        macroParams = new HashMap<String, String>();
        space = new Space("ds");
        pageToRenderOn = new Page();
        pageToRenderOn.setSpace(space);
        macroVelocityContext = new HashMap<String, Object>();

        user = new ConfluenceUserImpl("admin", null, null);

        labelOne = new Label("labelone");
        labelOne.setId(1);

        personalLabelOne = new Label("my:labelone");
        personalLabelOne.setId(1);

        labelledContent = new Page();
        labelledContent.setId(1);
    }

    public void testDefaultResultCountIsTen() throws MacroException
    {
        final List<Label> labels = Arrays.asList(labelOne);

        when(labelManager.getRecentlyUsedLabels(10)).thenReturn(labels);

        final String fakeOutput = "fakeOutput";

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labels, contextMap.get("recentlyUsedLabels"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testSetTitle() throws MacroException
    {
        final String fakeOutput = "fakeOutput";
        final String title = "title";

        macroParams.put("title", title);

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(title, contextMap.get("title"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testResultCountCannotBeMoreThanOneHundred() throws MacroException
    {
        final List<Label> labels = Arrays.asList(labelOne);

        when(labelManager.getRecentlyUsedLabels(100)).thenReturn(labels);

        final String fakeOutput = "fakeOutput";

        macroParams.put("count", String.valueOf(Integer.MAX_VALUE));

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labels, contextMap.get("recentlyUsedLabels"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testSpaceScopeLimitsRecentLabelsOfParticularSpace() throws MacroException
    {
        final List<Label> labels = Arrays.asList(labelOne);

        when(labelManager.getRecentlyUsedLabelsInSpace(space.getKey(), 10)).thenReturn(labels);

        final String fakeOutput = "fakeOutput";

        macroParams.put("scope", "space");

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labels, contextMap.get("recentlyUsedLabels"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testPersonalScopeLimitsRecentLabelsOfParticularPerson() throws MacroException
    {
        final List<Label> labels = Arrays.asList(personalLabelOne);

        try
        {
            AuthenticatedUserThreadLocal.setUser(user);

            when(labelManager.getRecentlyUsedPersonalLabels(user.getName(), 10)).thenReturn(labels);

            final String fakeOutput = "fakeOutput";

            macroParams.put("scope", "personal");

            assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
            {
                @Override
                protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
                {
                    assertEquals(labels, contextMap.get("recentlyUsedLabels"));
                    return fakeOutput;
                }
            }.execute(macroParams, null, pageToRenderOn.toPageContext()));

        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }
    }

    public void testPersonalScopeOfRecentlyUsedLabelsDefaultsToGlobalForAnonymousUsers() throws MacroException
    {
        final List<Label> labels = Arrays.asList(personalLabelOne);

        when(labelManager.getRecentlyUsedLabels(10)).thenReturn(labels);

        final String fakeOutput = "fakeOutput";

        macroParams.put("scope", "personal");

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labels, contextMap.get("recentlyUsedLabels"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testDefaultScopeOfRecentLabellingsIsGlobal() throws MacroException
    {
        final List<Labelling> labellings = Arrays.asList(new Labelling(labelOne, labelledContent, user));

        when(labelManager.getRecentlyUsedLabellings(10)).thenReturn(labellings);

        final String fakeOutput = "fakeOutput";

        macroParams.put("scope", "personal");
        macroParams.put("style", "table");

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labellings, contextMap.get("recentlyUsedLabellings"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testSpaceScopeLimitsRecentLabellingsOfParticularSpaceInTabularFormat() throws MacroException
    {
        final List<Labelling> labellings = Arrays.asList(new Labelling(labelOne, labelledContent, user));

        when(labelManager.getRecentlyUsedLabellingsInSpace(space.getKey(), 10)).thenReturn(labellings);

        final String fakeOutput = "fakeOutput";

        macroParams.put("scope", "space");
        macroParams.put("style", "table");

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labellings, contextMap.get("recentlyUsedLabellings"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }


    public void testPersonalScopeLimitsRecentLabellingsOfParticularPerson() throws MacroException
    {
        final List<Labelling> labellings = Arrays.asList(new Labelling(personalLabelOne, labelledContent, user));

        try
        {
            AuthenticatedUserThreadLocal.setUser(user);

            when(labelManager.getRecentlyUsedPersonalLabellings(user.getName(), 10)).thenReturn(labellings);

            final String fakeOutput = "fakeOutput";

            macroParams.put("scope", "personal");
            macroParams.put("style", "table");

            assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
            {
                @Override
                protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
                {
                    assertEquals(labellings, contextMap.get("recentlyUsedLabellings"));
                    return fakeOutput;
                }
            }.execute(macroParams, null, pageToRenderOn.toPageContext()));

        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }
    }

    public void testPersonalScopeOfRecentLabellingsDefaultsToGlobalForAnonymousUsers() throws MacroException
    {
        final List<Labelling> labellings = Arrays.asList(new Labelling(personalLabelOne, labelledContent, user));

        when(labelManager.getRecentlyUsedLabellings(10)).thenReturn(labellings);

        final String fakeOutput = "fakeOutput";

        macroParams.put("scope", "personal");
        macroParams.put("style", "table");

        assertEquals(fakeOutput, new TestRecentlyUsedLabelsMacro()
        {
            @Override
            protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
            {
                assertEquals(labellings, contextMap.get("recentlyUsedLabellings"));
                return fakeOutput;
            }
        }.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testDoesNotRenderBody()
    {
        assertEquals(RenderMode.NO_RENDER, recentlyUsedLabelsMacro.getBodyRenderMode());
    }

    public void testHasNoBody()
    {
        assertFalse(recentlyUsedLabelsMacro.hasBody());
    }

    public void testRendersBlock()
    {
        assertFalse(recentlyUsedLabelsMacro.isInline());
    }

    private class TestRecentlyUsedLabelsMacro extends RecentlyUsedLabelsMacro
    {
        private TestRecentlyUsedLabelsMacro()
        {
            setLabelManager(labelManager);
            setUserAccessor(userAccessor);
            setFormatSettingsManager(formatSettingsManager);
        }

        @Override
        protected Map<String, Object> getMacroVelocityContext()
        {
            return macroVelocityContext;
        }
    }
}
