package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchWithToken;
import org.junit.Test;

public class RecentChangesSearchBuilderTestCase extends AbstractTestCase
{
    private RecentChangesSearchBuilder builder;

    public void testPaginationParametersRespected()
    {
        SearchWithToken search = builder.withStartIndex(2).withPageSize(5).withSearchToken(123).buildSearchWithToken();

        assertEquals(2, search.getStartOffset());
        assertEquals(5, search.getLimit());
        assertEquals(123, search.getSearchToken());
    }

    public void testBuildSearchUrlStartIndex() throws Exception
    {
        assertFalse(builder.withStartIndex(0).buildSearchUrl(null, null).contains("startIndex")); // no need to output for default value

        assertTrue(builder.withStartIndex(1).buildSearchUrl(null, null).contains("startIndex=1"));
    }

    public void testBuildSearchUrlPageSize() throws Exception
    {
        assertFalse(builder.withPageSize(RecentChangesSearchBuilder.DEFAULT_SIZE).buildSearchUrl(null, null).contains("pageSize")); // no need to output for default value

        assertTrue(builder.withPageSize(1).buildSearchUrl(null, null).contains("pageSize=1"));
    }

    public void testWithPageSize()
    {
        try
        {
            builder.withPageSize(0);
            fail();
        }
        catch (IllegalArgumentException e)
        {
        }
        try
        {
            builder.withPageSize(-1);
            fail();
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testWithSearchToken()
    {
        try
        {
            builder.withSearchToken(0);
            fail();
        }
        catch (IllegalArgumentException e)
        {
        }
        try
        {
            builder.withSearchToken(-1);
            fail();
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testWithStartIndex()
    {
        try
        {
            builder.withStartIndex(-1);
            fail();
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testBuildSearchUrlSearchToken()
    {
        assertFalse(builder.buildSearchUrl(null, null).contains("searchToken="));

        assertTrue(builder.withSearchToken(123).buildSearchUrl(null, null).contains("searchToken=123"));
    }

    public void testBuildSearchUrlRespectsContextPath()
    {
        assertTrue(builder.buildSearchUrl(null, null).startsWith("/plugins/recently-updated/changes.action"));
        assertTrue(builder.buildSearchUrl(null, "/confluence").startsWith("/confluence/plugins/recently-updated/changes.action"));
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        builder = new RecentChangesSearchBuilder();
    }
}
