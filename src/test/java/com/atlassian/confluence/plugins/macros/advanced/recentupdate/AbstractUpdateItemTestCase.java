package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

public class AbstractUpdateItemTestCase extends AbstractTestCase
{
    private AbstractUpdateItem updateItem;
    private final String title = "<b>hello</b>";
    private String contextPath = "/confluence";

    @Mock private SearchResult searchResult;
    @Mock private I18NBean i18n;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, contextPath));

        updateItem = new AbstractUpdateItem(searchResult, null, i18n, "") {
            protected String getDescriptionAndDateKey()
            {
                return null;
            }

            protected String getDescriptionAndAuthorKey()
            {
                return null;
            }
        };

        when(searchResult.getDisplayTitle()).thenReturn(title);
    }

    @Override
    protected Map<String, ?> getAdditionalSingletons()
    {
        return Collections.singletonMap("i18NBean", i18n);
    }

    public void testDisplayTitleHtmlEncoded()
    {
        assertEquals(GeneralUtil.htmlEncode(title), updateItem.getUpdateTargetTitle());
    }

    public void testLinkedUpdateTarget()
    {
        String urlPath = "/space/homepage";

        when(searchResult.getUrlPath()).thenReturn(urlPath);

        assertEquals("<a href=\"" + contextPath + urlPath + "\" title=\"\">" + GeneralUtil.htmlEncode(title) + "</a>",
                updateItem.getLinkedUpdateTarget());
    }

    public void testLinkedSpace()
    {
        String spaceKey = "TST";
        String spaceName = "Test Space";

        when(searchResult.getSpaceKey()).thenReturn(spaceKey);
        when(searchResult.getSpaceName()).thenReturn(spaceName);

        assertEquals("<a href=\"" + contextPath + "/display/" + spaceKey + "\">" + spaceName + "</a>", updateItem.getLinkedSpace());
    }
}
