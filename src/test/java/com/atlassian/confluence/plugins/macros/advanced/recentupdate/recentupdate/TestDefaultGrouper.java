package com.atlassian.confluence.plugins.macros.advanced.recentupdate.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.*;
import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResult;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.confluence.userstatus.InlineWikiStyleRenderer;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

public class TestDefaultGrouper extends AbstractTestCase
{
    @MockitoAnnotations.Mock InlineWikiStyleRenderer inlineWikiStyleRenderer;
    @MockitoAnnotations.Mock ConfluenceUserDao confluenceUserDao;
    private DefaultUpdater fred;
    private DefaultGrouper grouper;

    public void testStatusesGroupedSeparately()
    {
        SearchResult searchResult = new LuceneSearchResult(Collections.singletonMap(DocumentFieldName.LAST_MODIFIER_NAME, fred.getUsername()));

        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));
        grouper.addUpdateItem(new UserStatusUpdateItem(searchResult, null, i18n, inlineWikiStyleRenderer, ""));
        grouper.addUpdateItem(new UserStatusUpdateItem(searchResult, null, i18n, inlineWikiStyleRenderer, ""));

        assertEquals(3, grouper.getUpdateItemGroupings().size());
    }

    public void testOtherUpdatesCannotBeAddedToStatusGrouping()
    {
        SearchResult searchResult = new LuceneSearchResult(Collections.singletonMap(DocumentFieldName.LAST_MODIFIER_NAME, fred.getUsername()));

        grouper.addUpdateItem(new UserStatusUpdateItem(searchResult, null, i18n, inlineWikiStyleRenderer, ""));
        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));

        assertEquals(2, grouper.getUpdateItemGroupings().size());
    }

    public void testItemsGroupedByUpdater()
    {
        SearchResult searchResult = new LuceneSearchResult(Collections.singletonMap(DocumentFieldName.LAST_MODIFIER_NAME, fred.getUsername()));

        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));
        grouper.addUpdateItem(new ContentUpdateItem(searchResult, null, i18n, ""));
        grouper.addUpdateItem(new CommentUpdateItem(searchResult, null, i18n, ""));

        final List<Grouping> groupings = grouper.getUpdateItemGroupings();
        assertEquals(1, groupings.size());
        assertEquals(3, groupings.get(0).size());
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        grouper = new DefaultGrouper();
        fred = new DefaultUpdater("fred", i18n);

        when(confluenceUserDao.findByUsername("fred")).thenReturn(new ConfluenceUserImpl("fred", null, null));

        ContainerContext containerContext = Mockito.mock(ContainerContext.class);  
        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("i18NBean")).thenReturn(null);
        when(containerContext.getComponent("confluenceUserDao")).thenReturn(confluenceUserDao);
    }
}
