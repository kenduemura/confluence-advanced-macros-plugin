package com.atlassian.confluence.plugins.macros.advanced.xhtml.migration;

import com.atlassian.confluence.xhtml.api.MacroDefinition;
import junit.framework.TestCase;

import java.util.Collections;

public class GalleryMacroMigrationTest extends TestCase
{
    private GalleryMacroMigration galleryMacroMigration;

    public void testMigrateReverseSortParameter() throws Exception
    {
        MacroDefinition definition = new MacroDefinition("foo", null, null, Collections.singletonMap("reverseSort", ""));

        MacroDefinition migratedMacroDefinition = galleryMacroMigration.migrate(definition, null);
        assertEquals("true", migratedMacroDefinition.getParameters().get("reverse"));
        assertFalse(migratedMacroDefinition.getParameters().containsKey("reverseSort"));
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        galleryMacroMigration = new GalleryMacroMigration();
    }
}
