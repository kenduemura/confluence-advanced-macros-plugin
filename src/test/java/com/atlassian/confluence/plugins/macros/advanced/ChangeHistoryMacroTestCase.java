package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import junit.framework.TestCase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChangeHistoryMacroTestCase extends TestCase
{
	private static String ERROR_TEXT = "changehistory.error.can-only-be-used-in-pages-or-blogposts";
	
	private PageManager pageManager;
	private PageContext pageContext;
	private Map<String, Object> parameters, contextMap;
	private String returnText = "Execution Done";
	private Page page, pageTwo;
	
	private ChangeHistoryMacro changeHistoryMacro;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		page = new Page();
		pageTwo = new Page();
		
		pageManager = mock(PageManager.class);
		pageContext = mock(PageContext.class);
		
		parameters = new HashMap<String, Object>();
		contextMap = new HashMap<String, Object>();
		
		changeHistoryMacro = new ChangeHistoryMacro()
					{
						@Override
						protected Map<String, Object> getDefaultVelocityContext() {
							return contextMap;
						}
							
						@Override
						protected String getRenderedTemplate(
								Map<String, Object> contextMap) {
							
							AbstractPage currentVersion = (AbstractPage) contextMap.get("page");
							assertEquals("PageOne Title", currentVersion.getTitle());
							assertEquals("This is Page Content", currentVersion.getBodyAsString());
							
							List previousVersion = (List) contextMap.get("previousVersions");
							assertEquals(1, ((List) contextMap.get("previousVersions")).size());
							assertEquals("PageTwo Title", ((Page) previousVersion.get(0)).getTitle());
							assertEquals("This is PageTwo Content", ((Page) previousVersion.get(0)).getBodyAsString());
							
							return returnText;
						}

						@Override
						protected String getConfluenceActionSupportTextStatic() {
							return ERROR_TEXT;
						}
					};
		changeHistoryMacro.setPageManager(pageManager);
	}
	
	public void testExecuteMethodWhenContentEntityObejctIsNotAnInstanceOfAbstractPage() throws MacroException
	{
		String errorMessage = "<div class=\"error\"><span class=\"error\">" + ERROR_TEXT + "</span> </div>";
		
		when(pageContext.getEntity()).thenReturn(null);
		
		assertEquals(errorMessage, changeHistoryMacro.execute(parameters, "", pageContext));
	}
	
	public void testIsInline()
	{
	    assertEquals(false, changeHistoryMacro.isInline());
	}
	
	public void testHasBody()
    {
        assertEquals(false, changeHistoryMacro.hasBody());
    }
	
	public void testBodyRenderMode()
    {
        assertEquals(RenderMode.NO_RENDER, changeHistoryMacro.getBodyRenderMode());
    }
}
