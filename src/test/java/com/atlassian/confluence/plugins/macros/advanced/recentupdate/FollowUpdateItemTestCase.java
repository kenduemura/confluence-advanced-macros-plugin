package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.Collections;

public class FollowUpdateItemTestCase extends AbstractTestCase
{
    private FollowUpdateItem followUpdateItem;
    
    @Mock private SearchResult searchResult;
    @Mock private DateFormatter dateFormatter;
    @Mock private PersonalInformation followeePersonalInfo;

    public void testLinkedUpdateTargetUrl()
    {
        final String contextPath = "/confluence";
        String urlPath = "/space/home/page";
        String title = "This is title";
        RequestCacheThreadLocal.setRequestCache(Collections.singletonMap(RequestCacheThreadLocal.CONTEXT_PATH_KEY, contextPath));

        String follower = "";
        
        followUpdateItem = new FollowUpdateItem(searchResult, dateFormatter, followeePersonalInfo, follower, i18n);

        when(followeePersonalInfo.getUrlPath()).thenReturn(urlPath);
        when(followeePersonalInfo.getDisplayTitle()).thenReturn(title);
        
        assertEquals("<a href=\"" + contextPath + urlPath + "\">" + title + "</a>", 
                followUpdateItem.getLinkedUpdateTarget());
    }
}
