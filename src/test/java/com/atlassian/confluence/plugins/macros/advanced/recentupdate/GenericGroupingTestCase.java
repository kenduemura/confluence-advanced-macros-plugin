package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenericGroupingTestCase extends AbstractTestCase
{
    private GenericGrouping genericGrouping;
    private DefaultUpdater fred;

    public void testAddUserStatusItemProhibited()
    {
        assertFalse(genericGrouping.canAdd(mock(UserStatusUpdateItem.class)));
    }

    public void testCannotAddUpdatesFromMultipleUsersInTheOneGrouping()
    {
        final ContentUpdateItem fredsUpdate1 = createUpdateFor(fred);
        final ContentUpdateItem fredsUpdate2 = createUpdateFor(fred);
        final ContentUpdateItem barneysUpdate = createUpdateFor(new DefaultUpdater("barney", i18n));
        genericGrouping.addUpdateItem(fredsUpdate1);

        assertFalse(genericGrouping.canAdd(barneysUpdate));
        assertTrue(genericGrouping.canAdd(fredsUpdate2));
    }

    private ContentUpdateItem createUpdateFor(DefaultUpdater fred)
    {
        final ContentUpdateItem fredsUpdate = mock(ContentUpdateItem.class);
        when(fredsUpdate.getUpdater()).thenReturn(fred);
        return fredsUpdate;
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        fred = new DefaultUpdater("fred", i18n);
        genericGrouping = new GenericGrouping(fred);
    }
}
