package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.v2.SearchResult;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

public class UpdateItemUtilsTestCase extends AbstractTestCase
{
    private Map<String, String> map;
    @Mock private SearchResult searchResult;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        map = new HashMap<String, String>();
        when(searchResult.getExtraFields()).thenReturn(map);
    }

    public void testGetContentVersionFromSearchResult()
    {
        map.put("content-version", "2");
        assertEquals(2, UpdateItemUtils.getContentVersion(searchResult));
    }
    
    public void testGetDefaultContentVersion()
    {
        assertEquals(-1, UpdateItemUtils.getContentVersion(searchResult));
    }
}
