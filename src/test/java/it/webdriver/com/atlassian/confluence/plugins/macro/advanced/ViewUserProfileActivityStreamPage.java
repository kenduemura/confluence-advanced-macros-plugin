package it.webdriver.com.atlassian.confluence.plugins.macro.advanced;

import com.atlassian.confluence.pageobjects.page.user.ViewUserProfilePage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;

import javax.inject.Inject;

public class ViewUserProfileActivityStreamPage extends ViewUserProfilePage
{
    @ElementBy(className = "recently-updated") ActivityStreamElement activityStreamElement;

    public ActivityStreamElement getActivityStream()
    {
        return activityStreamElement;
    }
}
