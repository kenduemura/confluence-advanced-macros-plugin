package it.webdriver.com.atlassian.confluence.plugins.macro.advanced;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.WebDriverLocatable;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.annotation.Nullable;
import java.util.List;

public class ActivityStreamElement extends WebDriverElement
{
    @ElementBy(className = "more-link") PageElement moreLink;

    public ActivityStreamElement(By locator)
    {
        super(locator);
    }

    public ActivityStreamElement(By locator, TimeoutType timeoutType)
    {
        super(locator, timeoutType);
    }

    public ActivityStreamElement(By locator, WebDriverLocatable parent)
    {
        super(locator, parent);
    }

    public ActivityStreamElement(By locator, WebDriverLocatable parent, TimeoutType timeoutType)
    {
        super(locator, parent, timeoutType);
    }

    public ActivityStreamElement(WebDriverLocatable locatable, TimeoutType timeoutType)
    {
        super(locatable, timeoutType);
    }

    public PageElement getMoreLink()
    {
        return moreLink;
    }

    public TimedQuery<Iterable<String>> getStreamTitles()
    {
        return Queries.forSupplier(timeouts, new Supplier<Iterable<String>>()
        {
            @Override
            public Iterable<String> get()
            {
                return Iterables.transform(ActivityStreamElement.this.findAll(By.cssSelector(".update-item .update-item-details div a")), new Function<PageElement, String>()
                {
                    @Override
                    public String apply(PageElement input)
                    {
                        return input.getText();
                    }
                });
            }
        });
    }

}
