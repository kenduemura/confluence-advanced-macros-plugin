package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;

public class FavouritePagesMacroFuncTest extends AbstractConfluencePluginWebTestCaseBase
{
    private void markSpaceAsFavorite(String spaceKey)
    {
        gotoPage("/spaces/viewspacesummary.action?key=" + spaceKey);
        String token = getTester().getElementAttributByXPath("id('atlassian-token')", "content");

        PostMethod post = new PostMethod("/json/addspacetofavourites.action");
        post.addParameter("key", spaceKey);
        post.addParameter("alt_token", token);

        try
        {
            post.getResponseBodyAsString();
        }
        catch (IOException e)
        {

        }
    }

    private void markUserAsFavorite(String username)
    {
        gotoPage("/addprofiletofavourites.action?username=" + username);
    }

    private void assertFavoriteTablePreamble()
    {
        assertEquals(
                "your favourites list",
                getElementTextByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr/td/a")
        );
        assertEquals(
                "Displaying pages recently added to your favourites list.",
                getElementTextByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr/td")
        );
    }

    private void assertFavoriteRow(int rowIndex, String contentTitle, String spaceKey, boolean isBlog)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);

        assertTrue(spaceHelper.read());

        String spaceName = spaceHelper.getName();

        String contentIconClass = getElementAttributByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[" + (rowIndex + 1) + "]/td/span[contains(@class, 'icon')]", "class");

        assertTrue(
                contentIconClass.contains(
                        isBlog ? "icon-blog" : "icon-page"
                )
        );

        assertEquals(
                contentTitle,
                getElementTextByXPath(
                        "//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[" + (rowIndex + 1) + "]/td/a"
                )
        );
        assertEquals(
                "(" + spaceName + ")",
                getElementTextByXPath(
                        "//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[" + (rowIndex + 1) + "]/td/span[@class='smalltext']"
                )
        );
        assertEquals(
                "Delete this page from your favourites list",
                getElementAttributeByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[" + (rowIndex + 1) + "]/td[2]/a", "title")
        );
    }

    public void testFavoritePagesShowOnlyPagesOrBlogPostsAcrossAllSpaces()
    {
        String blogTitle = "favorite blog";
        long favBlogId = createBlogPost(DEMO_SPACE_KEY, blogTitle, StringUtils.EMPTY);

        BlogPostHelper blogPostHelper = getBlogPostHelper(favBlogId);
        assertTrue(blogPostHelper.read());

        blogPostHelper.markFavourite();

        String favPageTitle = "testFavoritePagesShowOnlyPagesOrBlogPostsAcrossAllSpaces";
        long favPageId = createPage(TEST_SPACE_KEY, favPageTitle, "{favpages}");

        PageHelper pageHelper = getPageHelper(favPageId);
        assertTrue(pageHelper.read());

        pageHelper.markFavourite();

//        markSpaceAsFavorite(TEST_SPACE_KEY);
        //Temporary commented out as latest Confluence milestone doesn't have add favorite link anymore
        //markUserAsFavorite(getConfluenceWebTester().getCurrentUserName());

        viewPageById(favPageId);

        assertFavoriteTablePreamble();
        assertFavoriteRow(1, favPageTitle, TEST_SPACE_KEY, false);
        assertFavoriteRow(2, blogTitle, DEMO_SPACE_KEY, true);
    }

    public void testFavoriteMaxResultsDefaultsToFive()
    {
        for (int i = 0; i < 6; ++i)
        {
            PageHelper pageHelper = getPageHelper();

            pageHelper.setSpaceKey(TEST_SPACE_KEY);
            pageHelper.setTitle(String.valueOf(i));
            pageHelper.setContent(StringUtils.EMPTY);

            assertTrue(pageHelper.create());

            pageHelper.markFavourite();
        }



        String testPageTitle = "testFavoriteMaxResultsDefaultsToFive";
        long testPageId = createPage(TEST_SPACE_KEY, testPageTitle, "{favpages}");

        viewPageById(testPageId);

        assertElementPresentByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[6]");
        assertElementNotPresentByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[7]");
    }

    public void testFavoritesLimitedByMaxResults()
    {
        String blogTitle = "favorite blog";
        long favBlogId = createBlogPost(DEMO_SPACE_KEY, blogTitle, StringUtils.EMPTY);

        BlogPostHelper blogPostHelper = getBlogPostHelper(favBlogId);
        assertTrue(blogPostHelper.read());

        blogPostHelper.markFavourite();

        String favPageTitle = "testFavoritesLimitedByMaxResults";
        long favPageId = createPage(TEST_SPACE_KEY, favPageTitle, "{favpages:maxResults=1}");

        PageHelper pageHelper = getPageHelper(favPageId);
        assertTrue(pageHelper.read());

        pageHelper.markFavourite();

        viewPageById(favPageId);

        assertElementPresentByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[2]");
        assertElementNotPresentByXPath("//div[@class='tabletitle']//h2[text()='Favourite Pages']/../../div/table[@class='tableview']//tr[3]");
    }

    public void testShowFavoritesFromMacro()
    {
        String testPageTitle = "testShowFavoritesFromMacro";
        long favPageId = createPage(TEST_SPACE_KEY, testPageTitle, "{favpages}");

        PageHelper pageHelper = getPageHelper(favPageId);
        assertTrue(pageHelper.read());

        pageHelper.markFavourite();

        viewPageById(favPageId);

        clickLinkWithText("your favourites list");

        assertLinkPresentWithText(testPageTitle);
    }
}
