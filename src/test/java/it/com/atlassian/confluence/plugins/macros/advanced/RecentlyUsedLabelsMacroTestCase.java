package it.com.atlassian.confluence.plugins.macros.advanced;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class RecentlyUsedLabelsMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    private long createPageWithLabels(String spaceKey, String pageTitle, String content, int labelCount)
    {
        Collection<String> labels = new HashSet<String>(labelCount);

        for (int i = 0; i < labelCount; ++i)
            labels.add(String.valueOf("label-" + i));

        return createPageWithLabels(spaceKey, pageTitle, content, labels.toArray(new String[labelCount]));
    }

    private long createPageWithLabels(String spaceKey, String pageTitle, String content, String ...labels)
    {
        return createPage(spaceKey, pageTitle, content, Arrays.asList(labels));
    }



    private void assertConfluenceLabelPresent(String label, boolean present)
    {
        if (present)
            assertElementPresentByXPath("//div[@class='wiki-content']/a[text()='" + label + "']");
        else
            assertElementNotPresentByXPath("//div[@class='wiki-content']/a[text()='" + label + "']");
    }

    private void assertConfluenceLabelPresent(String label)
    {
        assertConfluenceLabelPresent(label, true);
    }

    public void testDefaultMacroCountIsTen()
    {
        long testPageId = createPageWithLabels(
                TEST_SPACE_KEY,
                "testDefaultMacroCountIsTen",
                "{recently-used-labels}", 11);

        getIndexHelper().update();
        viewPageById(testPageId);

        /* The order of recently used labels returned by the LabelManager is not defined... */
        assertElementPresentByXPath("//div[@class='wiki-content']/a[@class='label'][10]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/a[@class='label'][11]");
    }

    public void testLabelsReturnedCannotBeMoreThanOneHundred()
    {
        long testPageId = createPageWithLabels(
                TEST_SPACE_KEY,
                "testLabelsReturnedCannotBeMoreThanOneHundred",
                "{recently-used-labels:count=101}", 101);

        getIndexHelper().update();
        viewPageById(testPageId);

        /* The order of recently used labels returned by the LabelManager is not defined... */
        assertElementPresentByXPath("//div[@class='wiki-content']/a[@class='label'][100]");
        assertElementNotPresentByXPath("//div[@class='wiki-content']/a[@class='label'][101]");
    }

    public void testOnlyPersonalLabelsReturnedIfScopeIsPersonal()
    {
        createPageWithLabels(
                TEST_SPACE_KEY,
                "pageWithGlobalLabels",
                StringUtils.EMPTY, 10
        );

        String personalLabel1 = "my:personal1";
        String personalLabel2 = "my:personal2";
        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testOnlyPersonalLabelsReturnedIfScopeIsPersonal",
                "{recently-used-labels:scope=personal}",
                Arrays.asList(
                        personalLabel1, personalLabel2
                )
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(personalLabel1);
        assertConfluenceLabelPresent(personalLabel2);
        assertElementNotPresentByXPath("//div[@class='wiki-content']/a[@class='label'][3]");
    }

    public void testOnlyCurrentSpaceLabelsReturnedIfScopeIsSpace()
    {
        createPageWithLabels(
                DEMO_SPACE_KEY,
                "pageWithGlobalLabels",
                StringUtils.EMPTY, 10
        );

        String spaceLabel1 = "spacelabel1";
        long testPageId = createPage(
                TEST_SPACE_KEY,
                "testOnlyCurrentSpaceLabelsReturnedIfScopeIsSpace",
                "{recently-used-labels:scope=space}",
                Arrays.asList(
                        spaceLabel1
                )
        );

        viewPageById(testPageId);

        assertConfluenceLabelPresent(spaceLabel1);
        assertElementNotPresentByXPath("//div[@class='wiki-content']/a[@class='label'][3]");
    }

    private void assertRecentlyUsedLabelRow(
            int rowIndex,
            String label,
            Date useDate,
            String contentTitle,
            String creatorFullName)
    {
        assertEquals(
                label,
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (rowIndex + 1) + "]/td//a[@class='label']")
        );

        assertEquals(
                contentTitle,
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (rowIndex + 1) + "]/td/a[2]")
        );

        assertEquals(
                creatorFullName,
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (rowIndex + 1) + "]/td/span/a")
        );

        // Deliberately omit the minutes from the timestamp that we check for, to prevent intermittent
        // test failures due to clock skew between test and server. This takes advantage of the fact that the
        // minutes are the last thing in the table cell, so we can use a startsWith matcher.
        final String expectedTimestamp = FastDateFormat.getInstance("MMM dd, yyyy").format(useDate);
        assertThat(
                getElementTextByXPath("//div[@class='wiki-content']/table//tr[" + (rowIndex + 1) + "]/td"),
                startsWith(label + " added to " + contentTitle + " by " + creatorFullName + "\n" + expectedTimestamp)
        );
    }

    public void testRecentlyUsedLabelsInTableStyleWithBlogsAndPagesFromMultipleSpaces()
    {
        for (int i = 0; i < 2; ++i)
            createPage(TEST_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList("label-" + String.valueOf(i)));

        long blogPostId = createBlogPost(TEST_SPACE_KEY, "2", StringUtils.EMPTY);
        BlogPostHelper blogPostHelper = getBlogPostHelper(blogPostId);

        assertTrue(blogPostHelper.read());
        blogPostHelper.setLabels(Arrays.asList("label-2"));
        assertTrue(blogPostHelper.update());

        for (int i = 3; i < 5; ++i)
            createPage(DEMO_SPACE_KEY, String.valueOf(i), StringUtils.EMPTY, Arrays.asList("label-" + String.valueOf(i)));

        long testPageId = createPage(TEST_SPACE_KEY, "testRecentlyUsedLabelsInTableStyleWithBlogsAndPagesFromMultipleSpaces", "{recently-used-labels:style=table}");

        viewPageById(testPageId);

        Date labelUseDate = new Date();
        UserHelper userHelper = getUserHelper(getConfluenceWebTester().getCurrentUserName());

        assertTrue(userHelper.read());

        String currentUserFullName = userHelper.getFullName();

        assertRecentlyUsedLabelRow(0, "label-4", labelUseDate, "4", currentUserFullName);
        assertRecentlyUsedLabelRow(1, "label-3", labelUseDate, "3", currentUserFullName);
        assertRecentlyUsedLabelRow(2, "label-2", labelUseDate, "2", currentUserFullName);
        assertRecentlyUsedLabelRow(3, "label-1", labelUseDate, "1", currentUserFullName);
        assertRecentlyUsedLabelRow(4, "label-0", labelUseDate, "0", currentUserFullName);
    }
}
