package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase
{

    protected static final String TITLE_SEPARATOR = " - ";
    protected static final String SITE_TITLE_SUFFIX = TITLE_SEPARATOR + "Confluence";

    protected static final String TEST_SPACE_TITLE = "Test Space";
    protected static final String TEST_SPACE_KEY = "tst";
    protected static final String TEST_SPACE_DESCRIPTION = "Test Space";

    protected static final String DEMO_SPACE_KEY = "ds";
    protected static final String TEST_PAGE_TITLE = "Test page";

    public AbstractConfluencePluginWebTestCaseBase()
    {
        super();
    }

    @Override protected void setUp()
        throws Exception
    {
        super.setUp();

        createSpace(TEST_SPACE_KEY, TEST_SPACE_TITLE, TEST_SPACE_DESCRIPTION);
    }

    protected long createSpace(String spaceKey, String title, String description)
    {
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(spaceKey);
        spaceHelper.setName(title);
        spaceHelper.setDescription(description);
        assertTrue(spaceHelper.create());

        return spaceHelper.getHomePageId();
    }

    protected void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }

    protected void createUser(String username, String password, String fullname)
    {
        createUser(username, password, fullname, null);
    }

    protected void createUser(String username, String password, String fullname, String[] groups)
    {
        UserHelper user = getUserHelper(username);

        user.setPassword(password);
        user.setFullName(fullname);
        user.setEmailAddress(username + "@atlassian.com");

        if (null != groups)
            user.setGroups(Arrays.asList(groups));

        assertTrue(user.create());
    }

    protected void deleteUser(String username)
    {
        UserHelper user = getUserHelper(username);
        assertTrue(user.delete());
    }

    protected long createPage(String spaceKey, String title, String content, long parentId)
    {
        return createPage(spaceKey, parentId, title, content, new ArrayList());
    }

    protected long createPage(String spaceKey, String title, String content)
    {
        return createPage(spaceKey, 0, title, content, new ArrayList());
    }

    protected long createTestPage()
    {
        return createPage(TEST_SPACE_KEY, TEST_PAGE_TITLE, "");
    }

    protected long createPage(String spaceKey, String title, String content, List labels)
    {
        return createPage(spaceKey, 0, title, content, labels);
    }

    protected long createPage(String spaceKey, long parentId, String title, String content, List labels)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(parentId);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    protected void deletePage(long pageId)
    {
        PageHelper helper = getPageHelper();

        helper.setId(pageId);
        assertTrue(helper.delete());
    }

    protected long createComment(long pageId, long parentCommentId, String commentContent)
    {
        CommentHelper helper = getCommentHelper();

        helper.setContentId(pageId);
        helper.setParentId(parentCommentId);
        helper.setContent(commentContent);

        assertTrue(helper.create());

        return helper.getId();
    }

    protected long createAttachment(long pageId, String filename, byte[] content, String contentType)
    {
        AttachmentHelper helper = getAttachmentHelper();

        helper.setParentId(pageId);
        helper.setFileName(filename);
        helper.setContent(content);
        helper.setContentLength(content.length);
        helper.setContentType(StringUtils.defaultString(contentType, "application/octet-stream"));

        assertTrue(helper.create());

        return helper.getId();
    }

    protected void updatePage(long pageId)
    {
        PageHelper helper = getPageHelper(pageId);
        assertTrue(helper.read());

        helper.setContent("\nEdited by " + getConfluenceWebTester().getCurrentUserName());

        assertTrue(helper.update());
    }

    protected void updatePage(long pageId, String newContent)
    {
        PageHelper helper = getPageHelper(pageId);
        assertTrue(helper.read());

        helper.setContent(newContent);

        assertTrue(helper.update());
    }

    protected void addLabelToPage(String label, long pageId)
    {
        PageHelper helper = getPageHelper(pageId);
        assertTrue(helper.read());

        helper.getLabels().add(label);

        assertTrue(helper.update());
    }

    protected void addLabelToBlogPost(String label, long postId)
    {
        BlogPostHelper helper = getBlogPostHelper(postId);
        assertTrue(helper.read());

        helper.getLabels().add(label);

        assertTrue(helper.update());
    }

    protected long createBlogPost(String spaceKey, String title, String content)
    {
        BlogPostHelper helper = getBlogPostHelper();

        helper.setSpaceKey(spaceKey);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        assertTrue(helper.create());

        // return the generated id for the new blog post
        return helper.getId();
    }
    
    protected long createBlogPost(String spaceKey, String title, String content, Date postDate)
    {
        BlogPostHelper helper = getBlogPostHelper();

        helper.setSpaceKey(spaceKey);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(postDate);
        assertTrue(helper.create());

        // return the generated id for the new blog post
        return helper.getId();
    }

    protected void editBlogPost(long id, String title, String content)
    {
        BlogPostHelper helper = getBlogPostHelper(id);
        assertTrue(helper.read());

        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());

        int version = helper.getVersion();
        helper.setVersion(version++);

        assertTrue(helper.update());
    }

    protected void viewPage(String spaceKey, String title)
    {
        gotoPage("display/" + spaceKey + "/" + title);
    }

    protected void viewPage(String spaceKey, String title, String expectedTitle)
    {
        gotoPage("display/" + spaceKey + "/" + title);
        // TODO test that the correct page got loaded
        assertTitleEquals(title, expectedTitle);
    }

    /**
     * Views the page for a Page or BlogPost (AbstractPage)
     * 
     * @param entityId
     *            the ID of the AbstractPage
     */
    protected void viewPageById(long entityId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }

    protected void grantViewSpacePermissionToUser(String spaceKey, String username)
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/spaces/spacepermissions.action?key=" + spaceKey);
            setWorkingForm("editspacepermissions");
            submit();
            setWorkingForm("editspacepermissions");
            setTextField("usersToAdd", username);
            submit();
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    protected void viewGlobalAdmin()
    {
        clickElementByXPath("//div[@id='header']//a[@id='administration-link']");
    }

    protected void viewGlobalPermissions()
    {
        viewGlobalAdmin();
        clickLinkWithText("Global Permissions");
    }

    protected void grantGlobalBrowsePermissionToGroup(String groupName)
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            viewGlobalPermissions();
            setWorkingForm("editglobalperms");
            submit();

            setWorkingForm("editglobalperms");
            setFormElement("groupsToAdd", groupName);
            submit();
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    /**
     * Asserts that the title equals the passed pageTitle plus separator plus
     * spaceTitle plus site suffix (usually SITE_TITLE_SUFFIX). <br>
     * e.g. "Home - Test Space - Confluence"
     * 
     * @param pageTitle
     *            The title displayed before the first separator
     * @param spaceTitle
     *            The title displayed for the tested space
     */
    protected void assertTitleEquals(String pageTitle, String spaceTitle)
    {
        super.assertTitleEquals(pageTitle + TITLE_SEPARATOR + spaceTitle + SITE_TITLE_SUFFIX);
    }

    /**
     * Asserts that all the given texts appear in the current response in the
     * order in which they appear in the array.
     * 
     * @param texts
     */
    protected void assertTextsPresentInOrder(String[] texts)
    {
        String responseText = getPageSource();

        int previousIndex = -1;
        for (int i = 0; i < texts.length; i++)
        {

            int currentIndex = responseText.indexOf(texts[i], previousIndex + 1);
            if (currentIndex == -1)
            {
                if (responseText.indexOf(texts[i]) == -1)
                    fail("Expected text [" + i + "] '" + texts[i] + "' not found in response '" + responseText + "'");

                fail("Expected text [" + i + "] '" + texts[i] + "' not found after text [" + (i - 1) + "] '" + texts[i - 1] + "' in response '" + responseText + "'");
            }

            previousIndex = currentIndex;
        }
    }

    /**
     * We use pause because mysql is not very good at distinguishing times that
     * are close together. The result is unpredictable ordering of search
     * results and intermittent failure of tests.
     * 
     * @param milliseconds
     *            the time to pause
     */
    protected void pause(long milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (InterruptedException e)
        {
            // noop.
        }
    }

    private File copyClassPathResourceToFile(String classPathResource) throws IOException
    {
        InputStream in = null;
        OutputStream out = null;

        try
        {
            File tempFile = File.createTempFile("confluence.macros.advanced.testdata-", null);


            in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream(classPathResource));
            out = new BufferedOutputStream(new FileOutputStream(tempFile));

            IOUtils.copy(in, out);

            return tempFile;
        }
        finally
        {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }

    }

    protected File restoreDataFromZipClassPathResource(String classPath) throws IOException
    {
        File testDataCopy = copyClassPathResourceToFile(classPath);
        getConfluenceWebTester().restoreData(testDataCopy);

        return testDataCopy;
    }
}